mod fraction;
mod math;

use crate::fraction::Fraction;
use crate::math::bernoulli;
use anyhow::Result;
use std::fmt;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Compute statistics about Bernoulli trials.")]
struct Opt {
    #[structopt(short, help = "Number of trials")]
    n: f64,
    #[structopt(short, help = "Probability of a trial succeeding")]
    p: f64,
    #[structopt(
        short,
        help = "Exact number of successes to calculate a probability for"
    )]
    k: Option<f64>,
}

#[derive(Debug)]
struct Stats {
    all_successes: f64,
    all_failures: f64,
    at_least_one_success: f64,
    at_least_one_failure: f64,
    expected_value: f64,
    k_successes: Option<f64>,
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Expected number of successes:\t{}\n\
             All successes:\t{:.10} ({})\n\
             All failures:\t{:.10} ({})\n\
             At least one success:\t{:.10} ({})\n\
             At least one failure:\t{:.10} ({})\n\
             {}",
            self.expected_value,
            self.all_successes,
            Fraction::from(self.all_successes),
            self.all_failures,
            Fraction::from(self.all_failures),
            self.at_least_one_success,
            Fraction::from(self.at_least_one_success),
            self.at_least_one_failure,
            Fraction::from(self.at_least_one_failure),
            self.k_successes
                .map(|k| format!("k successes:\t{:.10} ({})", k, Fraction::from(k)))
                .unwrap_or_default()
        )
    }
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    let stats = compute_stats(opt)?;
    println!("{}", stats);
    Ok(())
}

fn compute_stats(opt: Opt) -> Result<Stats> {
    let expected_value = opt.n * opt.p;
    let all_successes = bernoulli(opt.n, opt.n, opt.p)?; // k = n successes
    let all_failures = bernoulli(opt.n, 0., opt.p)?; // k = 0 successes
    let at_least_one_success = 1. - all_failures;
    let at_least_one_failure = 1. - all_successes;
    Ok(Stats {
        expected_value,
        all_successes,
        all_failures,
        at_least_one_success,
        at_least_one_failure,
        k_successes: opt
            .k
            .map(|k| bernoulli(opt.n, k, opt.p).unwrap_or_default()),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq;

    #[test]
    fn test_compute_stats() {
        let stats = compute_stats(Opt {
            n: 5.,
            p: 0.33,
            k: Some(3.),
        })
        .unwrap();
        assert_approx_eq!(stats.all_successes, 0.0039135393);
        assert_approx_eq!(stats.all_failures, 0.1350125107);
        assert_approx_eq!(stats.at_least_one_success, 0.8649874893);
        assert_approx_eq!(stats.at_least_one_failure, 0.9960864607);
        assert_approx_eq!(stats.k_successes.unwrap(), 0.161321193);

        let stats = compute_stats(Opt {
            n: 5.,
            p: 0.33,
            k: None,
        });
        assert_eq!(stats.unwrap().k_successes, None);
    }
}
