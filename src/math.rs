use anyhow::{ensure, Result};

/**
 * Computes the probability of exactly `k` successes in `n` trials with
 * probability of success `p`.
 */
pub fn bernoulli(n: f64, k: f64, p: f64) -> Result<f64> {
    Ok(choose(n, k)? * p.powf(k) * (1. - p).powf(n - k))
}

pub fn choose(n: f64, k: f64) -> Result<f64> {
    Ok(fact(n)? / (fact(k)? * fact(n - k)?))
}

pub fn fact(n: f64) -> Result<f64> {
    ensure!(n >= 0., "factorial <0 is not defined");
    let mut total: i64 = 1;
    for n in 2..=(n as i64) {
        total *= n;
    }
    Ok(total as f64)
}

#[macro_export]
macro_rules! assert_approx_eq {
    ($a:expr, $b:expr) => {
        assert!(($a - $b).abs() < 1e-10);
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fact() {
        assert!(fact(-1.).is_err());
        assert_eq!(fact(0.).unwrap(), 1.0);
        assert_eq!(fact(1.).unwrap(), 1.0);
        assert_eq!(fact(5.).unwrap(), 120.);
    }

    #[test]
    fn test_choose() {
        assert!(choose(1., -1.).is_err());
        assert!(choose(-1., 1.).is_err());
        assert!(choose(3., 7.).is_err());
        assert_eq!(choose(1., 1.).unwrap(), 1.);
        assert_eq!(choose(7., 3.).unwrap(), 35.);
    }

    #[test]
    fn test_bernoulli() {
        assert_approx_eq!(bernoulli(5., 0., 0.33).unwrap(), 0.13501251069999992);
        assert_approx_eq!(bernoulli(5., 1., 0.33).unwrap(), 0.3324934964999999);
        assert_approx_eq!(bernoulli(5., 2., 0.33).unwrap(), 0.3275309069999999);
        assert_approx_eq!(bernoulli(5., 3., 0.33).unwrap(), 0.16132119299999997);
        assert_approx_eq!(bernoulli(5., 4., 0.33).unwrap(), 0.0397283535);
    }
}
