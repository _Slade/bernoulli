use std::fmt;

#[derive(PartialEq, Debug)]
pub struct Fraction {
    n: f64,
    d: f64,
}

impl fmt::Display for Fraction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.0} / {:.0}", self.n, self.d)
    }
}

// Translated from https://stackoverflow.com/a/5128558/2884483
impl From<f64> for Fraction {
    fn from(mut x: f64) -> Self {
        let error = 0.000001;
        let n = x.floor();
        x -= n;
        if x < error {
            return Fraction { n, d: 1. };
        } else if 1. - error < x {
            return Fraction { n: n + 1., d: 1. };
        }

        // The lower fraction is 0/1
        let mut lower_n = 0.;
        let mut lower_d = 1.;
        // The upper fraction is 1/1
        let mut upper_n = 1.;
        let mut upper_d = 1.;
        loop {
            // The middle fraction is (lower_n + upper_n) / (lower_d + upper_d)
            let middle_n = lower_n + upper_n;
            let middle_d = lower_d + upper_d;
            // If x + error < middle
            if middle_d * (x + error) < middle_n {
                // middle is our new upper
                upper_n = middle_n;
                upper_d = middle_d;
            } else if middle_n < (x - error) * middle_d {
                lower_n = middle_n;
                lower_d = middle_d;
            } else {
                // Else middle is our best fraction
                return Fraction {
                    n: n * middle_d + middle_n,
                    d: middle_d,
                };
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_float_to_fraction() {
        for (n, d) in &[(1., 3.), (127., 93.), (1., 1.)] {
            assert_eq!(Fraction::from(n / d), Fraction { n: *n, d: *d });
        }
    }
}
