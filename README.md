# Bernoulli

A command-line utility for computing statistics of Bernoulli distributions. See `./bernoulli --help` for usage.
